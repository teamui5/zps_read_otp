sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/core/Fragment',
	"./BaseController"
], function (Controller, Fragment, BaseController) {
	"use strict";

	return BaseController.extend("fr.enedis.ReadOTP.ZPS_READ_OTP.controller.app", {
		onInit: function () {
			var oProjectsModel = new sap.ui.model.json.JSONModel({
				projects: [{
					id: 0,
					name: "projet-1",
					OTPs: [{
						idOTP: 1,
						name: "EOTP 1 - OPEX",
						value: 60.2,
						depenses: [{
								iDDepense: 1,
								cout: 25000,
								libelle: "Achat PC"
							}, {
								iDDepense: 2,
								cout: 15000,
								libelle: "Achat Ecran"
							}, {
								iDDepense: 3,
								cout: 3000,
								libelle: "Fournitures de bureaux"
							}, {
								iDDepense: 4,
								cout: 2000,
								libelle: "Achat fourniture de bureaux"
							}, {
								iDDepense: 5,
								cout: 50000,
								libelle: "Achat Bureau"
							}, {
								iDDepense: 6,
								cout: 30000,
								libelle: "Achat Chaise"
							},

						]
					}, {
						idOTP: 2,
						name: "EOTP 2 - CAPEX",
						value: 39.8,
						depenses: [{
								iDDepense: 7,
								cout: 8000,
								libelle: "Licences adobes"
							}, {
								iDDepense: 8,
								cout: 25000,
								libelle: "Licences Jetbrains"
							}, {
								iDDepense: 9,
								cout: 25000,
								libelle: "Office 365"
							}, {
								iDDepense: 10,
								cout: 3680,
								libelle: "Achat A"
							}, {
								iDDepense: 11,
								cout: 4500,
								libelle: "Achat B"
							}, {
								iDDepense: 12,
								cout: 36895,
								libelle: "Achat C"
							},

						]
					}]
				}, {
					id: 1,
					name: "projet-2",
					OTPs: [{
						idOTP: 3,
						name: "EOTP 3 - OPEX",
						value: 82.3,
						depenses: [{
								iDDepense: 13,
								cout: 85695,
								libelle: "Achat PC"
							}, {
								iDDepense: 14,
								cout: 25684,
								libelle: "Achat Ecran"
							}, {
								iDDepense: 15,
								cout: 2000,
								libelle: "Fournitures de bureaux"
							}, {
								iDDepense: 16,
								cout: 1000,
								libelle: "Achat fourniture de bureaux"
							}, {
								iDDepense: 17,
								cout: 95329,
								libelle: "Achat Bureau"
							}, {
								iDDepense: 18,
								cout: 12146,
								libelle: "Achat Chaise"
							},

						]
					}, {
						idOTP: 4,
						name: "EOTP 4 - CAPEX",
						value: 17.7,
						depenses: [{
								iDDepense: 19,
								cout: 8000,
								libelle: "Licences adobes"
							}, {
								iDDepense: 20,
								cout: 5869,
								libelle: "Licences Jetbrains"
							}, {
								iDDepense: 21,
								cout: 25000,
								libelle: "Office 365"
							}, {
								iDDepense: 22,
								cout: 5500,
								libelle: "Achat A"
							}, {
								iDDepense: 23,
								cout: 2563,
								libelle: "Achat B"
							}, {
								iDDepense: 24,
								cout: 1234,
								libelle: "Achat C"
							},

						]
					}]
				}]
			});
			this.setModel(oProjectsModel);

			this.interval = "";
			this.byId("comboOTP").setEnabled(false);
			this.byId("comboProject").setEnabled(false);
		},

		onClickCalendarButton: function (oEvent) {
			var oButton = oEvent.getSource();

			// create popover
			if (!this._oPopover) {
				Fragment.load({
					name: "fr.enedis.ReadOTP.ZPS_READ_OTP.view.calendarPopover",
					controller: this
				}).then(function (pPopover) {
					this._oPopover = pPopover;
					this.getView().addDependent(this._oPopover);
					this._oPopover.openBy(oButton);
				}.bind(this));
			} else {
				this._oPopover.openBy(oButton);
			}
		},

		onConfirmCalendar: function () {
			this._oPopover.close();
			this.byId("comboOTP").setEnabled(true);
			this.byId("comboProject").setEnabled(true);

		},

		onSelectProject: function (oEvent) {
			var comboOTP = this.byId("comboOTP");
			comboOTP.removeAllItems();
			comboOTP.setSelectedItem(null);
			this.byId("flbBarChart").setWidth("0%");
			this.byId('flbDonutChart').setWidth("50%");
			this.byId('donutChart').setVisible(true);
			this.byId('barChart').setVisible(false);
			var t = this.getModel().getData().projects;
			var id = parseInt(oEvent.getParameters().selectedItem.getProperty('key'));

			for (var i = 0; i < t.length; i++) {
				if (t[i].id == id) {
					this.byId('opexValue').setLabel(t[i].OTPs[0].name);
					this.byId('opexValue').setValue(t[i].OTPs[0].value);
					this.byId('opexValue').setDisplayedValue(t[i].OTPs[0].value);
					this.byId('capexValue').setLabel(t[i].OTPs[1].name);
					this.byId('capexValue').setValue(t[i].OTPs[1].value);
					this.byId('capexValue').setDisplayedValue(t[i].OTPs[1].value);
					this.project = t[i];
				}
			}

			var o = this.project.OTPs;

			for (var i = 0; i < o.length; i++) {
				var newItem = new sap.ui.core.Item({
					key: o[i].idOTP,
					text: o[i].name
				});
				comboOTP.addItem(newItem);
			}
		},

		onSelectOTP: function (oEvent) {
			this.byId('barChart').setVisible(true);
			this.byId("flbBarChart").setWidth("50%");
			this.byId('donutChart').setVisible(false);
			var oVizFrame = this.byId("barChart");

			this.OTPID = oEvent.getParameters().selectedItem.getProperty('key');
			for (var i = 0; i < this.project.OTPs.length; i++) {
				if (this.OTPID == this.project.OTPs[i].idOTP) {
					this.OTP = this.project.OTPs[i];
				}
			}
			var oDepensesModel = new sap.ui.model.json.JSONModel({
				depenses: this.OTP.depenses
			});

			oVizFrame.destroyDataset();
			oVizFrame.removeAllFeeds();

			var newDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: {
					name: "Nature de dépense",
					value: "{path : 'libelle'}"
				},
				measures: {
					name: "Montant en euro",
					value: "{path : 'cout'}"
				},
				data: {
					path: "/depenses"
				}
			});

			var feedValueAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "valueAxis",
				'type': "Measure",
				'values': ["Montant en euros"]
			});
			var feedCategoryAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "categoryAxis",
				'type': "Dimension",
				'values': ["Nature de dépense"]
			});

			oVizFrame.addFeed(feedValueAxis1);
			oVizFrame.addFeed(feedCategoryAxis1);
			oVizFrame.setDataset(newDataset);
			oVizFrame.setModel(oDepensesModel);

		}

	});
});