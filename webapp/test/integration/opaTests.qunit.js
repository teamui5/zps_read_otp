/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"fr/enedis/ReadOTP/ZPS_READ_OTP/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});